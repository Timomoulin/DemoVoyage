<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Destination>
 */
class DestinationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "nom"=>$this->faker->word(),
            "prix"=>$this->faker->numberBetween(200,2000),
            "estDisponible"=>$this->faker->boolean(),
            "pays_id"=>$this->faker->numberBetween(1,10)
        ];
    }
}
