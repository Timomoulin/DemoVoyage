<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lignes', function (Blueprint $table) {
            $table->foreignId("destination_id")->constrained("destinations")->onDelete("cascade");
            $table->foreignId("commande_id")->constrained("commandes")->onDelete("cascade");
            $table->integer("quantite");
            $table->primary(["destination_id", "commande_id"]);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lignes');
    }
};
