<?php

namespace Database\Seeders;

use App\Models\Commentaire;
use App\Models\Destination;
use App\Models\Note;
use App\Models\Pays;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        $admin=Role::create(["nom"=>"admin"]);

        $client=Role::create(["nom"=>"client"]);

        $moi=User::create([
            "name"=>"TMoulin",
            "email"=>"timomoulin@msn.com",
            "password"=>bcrypt("timomoulin@msn.com1"),
            "role_id"=>$admin->id]);

            $testClient=User::create([
                "name"=>"Test",
                "email"=>"test@email.com",
                "password"=>bcrypt("test@email.com1"),
                "role_id"=>$client->id]);


        $lesPays=Pays::factory(10)->create();
        $lesDestinations=Destination::factory(30)->create();

        $note1=Note::create([
            "score"=>4,
            "destination_id"=>1,
            "user_id"=>1
        ]);

        $note2=Note::create([
            "score"=>4,
            "destination_id"=>2,
            "user_id"=>1
        ]);

        $note3=Note::create([
            "score"=>2,
            "destination_id"=>1,
            "user_id"=>2
        ]);

        $note4=Note::create([
            "score"=>1,
            "destination_id"=>2,
            "user_id"=>2
        ]);

        $commentaire1=Commentaire::create([
            "texte"=>"Super destination !",
            "user_id"=>1,
            "destination_id"=>1
        ]);
        $commentaire2=Commentaire::create([
            "texte"=>"Bof",
            "user_id"=>2,
            "destination_id"=>1
        ]);


    }
}
