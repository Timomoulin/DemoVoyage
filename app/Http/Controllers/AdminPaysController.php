<?php

namespace App\Http\Controllers;

use App\Models\Pays;
use Illuminate\Http\Request;

class AdminPaysController extends Controller
{
    public function index(){
        $lesPays=Pays::all();
        return view("admin.pays.index",["desPays"=>$lesPays]);
    }

    public function create(){
        return view("admin.pays.create");
    }

    public function store(Request $request){

        $attributs=$request->validate([
            "nom"=>"required|min:2|max:255|unique:pays,nom|string",
            "region"=>"required",
            "drapeau"=>"image"
        ]);
        $image=$request->file("drapeau");
        $attributs["drapeau"]=$image->store("pays");

        $pays=Pays::create($attributs);
        session()->flash("success","Le pays a était ajouter !");
        return redirect("/admin/pays");

    }

    public function edit( Pays $pays){
        // $pays=Pays::findOrFail($id);
        return view("admin.pays.edit",["lePays"=>$pays]);
    }

    public function update(Request $request,Pays $pays){
        $attributs=$request->validate([
            "nom"=>"required|min:2|max:255|string|unique:pays,nom,".$pays->id,
            "region"=>"required",
            "drapeau"=>"image"
        ]);

        $image=$request->file("drapeau");
        if($image){
        $attributs["drapeau"]=$image->store("pays");
        }


        $pays->update($attributs);
        session()->flash("success","Modification reusite");
       return redirect("/admin/pays");
    }

    public function destroy(Pays $pays){
        $pays->delete();
        session()->flash("success","Suppresion reusite");
        return redirect("/admin/pays");
    }

    public function show(Pays $pays){
        return view("admin.pays.show",["pays"=>$pays]);
    }
}
