<?php

namespace App\Http\Controllers;

use App\Models\Destination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //TODO Pagination et recherche

        $querry=Destination::with(["pays"]);

        if(isset($request->nom))
        {
            $querry=$querry->where("nom","like","%".$request->input("nom")."%");
        }

        $lesDestinations=$querry->paginate(9);

        return view("visiteur.destinations.index",["lesDestinations"=>$lesDestinations]);
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function show(Destination $destination)
    {
       return view("visiteur.destinations.show",["destination"=>$destination]);
    }






}
