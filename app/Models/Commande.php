<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    use HasFactory;
    protected $guarded=["id"];

    public function utilisateur(){
return $this->belongsTo(User::class,"user_id");
    }

    public function lignes(){
        return $this->belongsToMany(Destination::class,"lignes")->using(Ligne::class)->withPivot("quantite")->withTimestamps();
    }
}
