<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    use HasFactory;


    public function pays(){
        return $this->belongsTo(Pays::class,"pays_id");
    }

    public function commentaires(){
        return $this->hasMany(Commentaire::class,"destination_id");
    }

    public function lignes(){
        return $this->belongsToMany(Commande::class,"lignes")->using(Ligne::class)->withPivot("quantite")->withTimestamps();
    }

    public function notes(){
        return $this->belongsToMany(User::class,"note")->using(Note::class)->withPivot("score")->withTimestamps();
    }
}
