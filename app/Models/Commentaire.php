<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
    use HasFactory;
    protected $guarded=["id"];

    public function auteur(){
        return $this->belongsTo(User::class,"user_id");
    }

    public function destination(){
        return $this->belongsTo(Destination::class,"destination_id");
    }
}
