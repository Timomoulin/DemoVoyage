<?php

use App\Http\Controllers\AdminDestinationController;
use App\Http\Controllers\AdminPaysController;
use App\Http\Controllers\DestinationController;
use App\Models\Pays;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contactus',function (){
    return view("formContact");
});

// Route::get('/test/{nom}', function ($nom){
//      dd($nom);
// }
// );

// Route::get('/admin/pays',[AdminPaysController::class,"index"]);
// Route::get("/admin/pays/create",[AdminPaysController::class,"create"]);
// Route::post("/admin/pays",[AdminPaysController::class,"store"]);
// Route::get("/admin/pays/{pays}/edit",[AdminPaysController::class,"edit"]);
// Route::put("/admin/pays/{pays}",[AdminPaysController::class,"update"]);
// Route::delete("/admin/pays/{pays}",[AdminPaysController::class,"destroy"]);
Route::resource("/admin/pays",AdminPaysController::class)->parameters(["pays"=>"pays"])->middleware("onlyAdmin");

Route::resource("admin/destinations",AdminDestinationController::class)->parameters(["destination"=>"destination"])->middleware("onlyAdmin");

Route::resource("/destinations",DestinationController::class)->only(["index","show"]);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
