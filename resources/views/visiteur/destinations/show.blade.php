@extends("template")
@section("titre")
{{$destination->nom}}
@endsection
@section("content")
<section>
    <h2>Informations</h2>
    Nom : {{$destination->nom}} <br>
    Prix : {{$destination->prix}} <br>
    {{-- Note : {{$destination->notes}} --}}
    Pays : {{$destination->pays->nom}}
</section>
<section>
    <h2>Commentaires</h2>
    @foreach ($destination->commentaires as $unCommentaire )
    <div class="row col-lg-6 col-md-8 mx-auto">
    <article class="mb-2 mx-auto bg-secondary text-light p-2 boder border-dark border-2 rounded">
        <b>{{$unCommentaire->auteur->name}}</b>
        <div>
            {{$unCommentaire->texte}}
        </div>
        <div class="text-muted">{{$unCommentaire->created_at}}</div>
    </article>
</div>
    @endforeach
</section>
@endsection
