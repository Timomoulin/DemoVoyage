
@extends("template")
@section("titre")
Destinations
@endsection
@section("content")
<h1>Destination</h1>
<div class="row col-lg-4 col-md-6 col-12 mx-auto">
    <h1></h1>
    <form action="/destinations" method="get">

        <div class="row mb-2">
            <label for="nom">nom</label>
            <input value="{{old("nom")}}" placeholder="Saisir nom " class="form-control" name="nom" id="nom" type="text">
        @error("nom")
            <div class="alert alert-danger my-2">
                    {{$message}}
            </div>
        @enderror
        </div>
        <button class="btn btn-primary">Envoyer</button>
    </form>
</div>

{{$lesDestinations->links()}}
<div class="row mt-2 row-cols-3 mx-auto">
@foreach ($lesDestinations as $uneDestination )

    <div class="card" style="width: 18rem;">
        <img src="..." class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">{{Str::upper($uneDestination->nom)}}</h5>
          <p class="card-text">
              <li>Prix : {{$uneDestination->prix}}</li>
              <li>Pays : {{$uneDestination->pays->nom}} </li>
          </p>
          <a href="/destinations/{{$uneDestination->id}}" class="btn btn-primary">Consulter</a>
        </div>
      </div>

@endforeach
</div>
@endsection
