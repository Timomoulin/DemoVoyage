@extends("template")
@section("titre")
Ajouter Pays
@endsection
@section("content")

<div class="row col-lg-4 col-md-6 col-12 mx-auto">
<h1>Ajouter un Pays</h1>
<form action="/admin/pays" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row mb-2">
        <label for="">Nom</label>
        <input value="{{old("nom")}}" placeholder="Saisir " class="form-control" name="nom" id="" type="text">
        @error("nom")
        <div class="alert alert-danger my-2">
            {{$message}}
        </div>
        @enderror
    </div>

 <div class="row mb-2">
     <label for="region">Region</label>
     <input value="{{old("region")}}" placeholder="Saisir region " class="form-control" name="region" id="region" type="text">
 @error("region")
     <div class="alert alert-danger my-2">
             {{$message}}
     </div>
 @enderror
 </div>

<div class="row mb-2">
    <label for="drapeau">drapeau</label>
    <input value="{{old("drapeau")}}" placeholder="Saisir drapeau " class="form-control" name="drapeau" id="drapeau" type="file">
@error("drapeau")
    <div class="alert alert-danger my-2">
            {{$message}}
    </div>
@enderror
</div>

    <button class="btn btn-primary">Envoyer</button>
</form>
</div>

@endsection
