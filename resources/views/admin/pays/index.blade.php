@extends("template")
@section("titre")
Pays
@endsection

@section("content")
<h1>Les pays</h1>
<a href="/admin/pays/create"  class="btn btn-success">Ajouter</a>
<table class="table">
    <thead>
        <th>Id</th>
        <th>Nom</th>
        <th>Region</th>
        <th>Drapeau</th>
        <th>Action</th>
    </thead>
    <tbody>
@foreach ($desPays as $unPays )
        <tr>
            <td>{{$unPays->id}}</td>
            <td>{{$unPays->nom}}</td>
            <td>{{$unPays->region}}</td>
            <td> <img src="/storage/{{$unPays->drapeau}}" alt=""></td>
            <td>
                <a href="/admin/pays/{{$unPays->id}}/edit"  class="btn btn-secondary">Modifier</a>
                <a href="/admin/pays/{{$unPays->id}}">Consulter</a>
                <form action="/admin/pays/{{$unPays->id}}" method="post">
                        @method("delete")
                        @csrf
                        <button class="btn btn-danger">Supprimer</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
