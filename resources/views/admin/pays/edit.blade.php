@extends("template")
@section("titre")
Modifier Pays
@endsection
@section("content")

<div class="row col-lg-4 col-md-6 col-12 mx-auto">
<h1>Modifier un Pays</h1>
<form action="/admin/pays/{{$lePays->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method("put")
    <div class="row mb-2">
        <label for="">Nom *</label>
        <input required minlength="2" value="{{old("nom") ?? $lePays->nom}}" placeholder="Saisir " class="form-control" name="nom" id="" type="text">
        @error("nom")
        <div class="alert alert-danger my-2">
            {{$message}}
        </div>
        @enderror
    </div>

 <div class="row mb-2">
     <label for="region">Region</label>
     <input value="{{old("region") ?? $lePays->region}}" placeholder="Saisir region " class="form-control" name="region" id="region" type="text">
 @error("region")
     <div class="alert alert-danger my-2">
             {{$message}}
     </div>
 @enderror
 </div>

 <div class="row mb-2">
     <label for="drapeau">Drapeau</label>
     <input value="" placeholder="Saisir drapeau " class="form-control" name="drapeau" id="drapeau" type="file">
 @error("drapeau")
     <div class="alert alert-danger my-2">
             {{$message}}
     </div>
 @enderror
 </div>


    <button class="btn btn-primary">Envoyer</button>
</form>
</div>

@endsection
