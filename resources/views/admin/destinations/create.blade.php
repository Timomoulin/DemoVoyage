@extends("template")
@section("titre")
Ajout Destination
@endsection
@section("content")
<div class="row col-lg-4 col-md-6 col-12 mx-auto">
    <h1>Ajout de destination</h1>
    <form action="" method="post">
        @csrf
        <div class="row mb-2">
            <label for="nom">Nom *</label>
            <input value="{{old("nom")}}" placeholder="Saisir nom " class="form-control" name="nom" id="nom" type="text">
        @error("nom")
            <div class="alert alert-danger my-2">
                    {{$message}}
            </div>
        @enderror
        </div>

        <div class="row mb-2">
            <label for="prix">Prix</label>
            <input value="{{old("prix")}}" placeholder="Saisir prix " class="form-control" name="prix" id="prix" type="number">
        @error("prix")
            <div class="alert alert-danger my-2">
                    {{$message}}
            </div>
        @enderror
        </div>

        <div class="row mb-2">
            <label for="estDisponible">Disponible</label>
            <input value="{{old("estDisponible")}}" placeholder="Saisir estDisponible "  name="estDisponible" id="estDisponible" type="checkbox">
        @error("estDisponible")
            <div class="alert alert-danger my-2">
                    {{$message}}
            </div>
        @enderror
        </div>

        <div class="row mb-2">
            <label for="pays_id">Pays</label>
            <select value="{{old("pays_id")}}" class="form-control" name="pays_id" id="pays_id">
                <option value="" selected disabled>Choisir un pays</option>
                @foreach ($lesPays as $unPays )
                <option value="{{$unPays->id}}">{{Str::ucfirst($unPays->nom)}}</option>
                @endforeach
            </select>
        @error("pays_id")
            <div class="alert alert-danger my-2">
                    {{$message}}
            </div>
        @enderror
        </div>
        <button class="btn btn-primary">Envoyer</button>
    </form>
</div>
@endsection
